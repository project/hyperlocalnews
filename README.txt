Hyperlocal news Drupal installation profile:
automates the process of creating a Drupal site for hyperlocal (e.g. neighborhoods) news dispersal. The site includes the following features:

* Map of locations
* Photo stream
* RSS feed
* Auto-tweeting of new content
* Visitors can sign up for a site newsletter
* Spam protection via Mollom
* Google Analytics to gauge site traffic

2010-03-06
This is the initial release of this profile. It does not yet completely conform to the style required for hosting at http://drupal.org/project/Installation%20profiles, yet is intended to conform and be hosted there.

The Hyperlocal News installation profile has been developed by Growing Venture Solutions, and the work is sponsored by Ryan Ornelas.

growingventuresolutions.com
ornelas.org.


Installation notes
=========================================
* Requires a modern Linux/Apache/MySQL/PHP hosting environment.
* Create a MySQL database for the site.
* Extract this archive into the web root (e.g. htdocs/example.com).
* Connect to the directory into which this site has been extracted.
* Load the site in a browser, an installation screen will display
  - select the Hyperlocal news profile (second radio button) and click the Install button
  - enter database information on the next screen; if your host's MySQL server is not the same as the web server, you'll need to open the Advanced section to specify the database server location
  
* Click Install -- installation will proceed, then a "Configure site" page will appear
  - enter the requested info -- this step creates the default account (user 1) which has permission to administer anything on the site

* After installation completes, you'll see a page "Hyperlocal news installation complete", plus a few taxonomy warnings that you can ignore

* Visit the home page -- directions there spell out the configuration steps. (The pink background on this information indicates that it is an unpublished node and will appear for administrators only.)



Development notes (not necessary for installation)
=======================================================
This installation profile was built using the following Drupal tools:

* Drush and Drush Make
* Features
* Exportables
* Install Profile API
* Strongarm
* cTools

To develop this installation, a set four features were exported. A drush make script provided the initial fileset (and will allow the profile to be hosted on Drupal.org). A site folder was built looking like this:

  hyperlocalnews
    LICENSE.txt     
    README.txt
    hyperlocal.make  
    hyperlocalnews.profile
    modules
      custom
        # four features-driven modules encapsulate most of the site architecture 
        hyperlocalnews_frontpage/
        hyperlocalnews_settings/
        hyperlocalnews_variables/
        hyperlocalnews_vocabularies/

