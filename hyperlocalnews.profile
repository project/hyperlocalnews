<?php

/**
 * Local community installation profile.
 *
 * Created by GVS, GrowingVentureSolutions.com.
 * Sponsored by Ryan Ornelas at Ornelas.org
 */

/**
* Return a description of the profile for the initial installation screen.
*
* @return
*   An array with keys 'name' and 'description' describing this profile.
*/
function hyperlocalnews_profile_details() {
  return array(
    'name' => 'Hyperlocal news',
    'description' => 'Build a basic hyperlocal news website.',
  );
}


/**
* Return an array of the modules to be enabled when this profile is installed.
*
* @return
*  An array of modules to be enabled.
*/
function hyperlocalnews_profile_modules() {
  return array(
    // Enable required core modules first.
    'block', 
    'filter', 
    'node', 
    'system', 
    'user', 
    
    // Enable optional core modules next.
    'comment', 
    'contact', 
    'dblog',  
    'help', 
    'menu', 
    'openid', 
    'path', 
    'profile', 
    'search', 
    'statistics', 
    'taxonomy', 
    'trigger',
    'tracker', 
    'update', 
    'upload',

    // Then, enable any contributed modules here.
    'jquery_ui',
    'install_profile_api',
    'views', 'views_ui',
    
    // Tools to build content types.
    'date_api',
    'date_timezone',
    'content',
    'filefield',
    'imagefield',
    'nodereference',
    'number',
    'optionwidgets',
    'text',
    'userreference',
    'imageapi',
    'imagecache',
    'imagecache_ui',
    'imageapi_gd',
    'date',
    'calendar',
    'markdown',
    'mollom',
    'popups',
    'popups_reference',
    
    // Geo.
    'location',
    'location_node',
    'gmap',
    'gmap_location',
    'location_search',
    
    
    // Outgoing communications.
    'simplenews',
    'smtp',
    // 'twitter',
    // 'twitter_actions',
    // 'twitter_post',
    'googleanalytics', 
    'comment_notify', 
    
    // Content management.
    'diff',
    'fasttoggle',
    
    // TODO: (low priority) not really needed, but required by a feature.
    'nodequeue',
    
    // TODO: (low priority) remove when the profile is "done"?
    'devel',

    // Misc.
    'advanced_help',
    'globalredirect',
    'path_redirect',
    'pathauto',
    'token',
        
    // Improve search.
    'porterstemmer',
    
    // Things general user never sees.
    'admin_menu',
  );
}

/*
 * Install some modules later to avoid installation hangs.
 */
function _hyperlocalnews_additional_modules() {
  return array(
    // Tools that glue the site together after install.
    'ctools', 
    'context_ui', 
    'features',
    'exportables', 
    'strongarm', 
    
    // Custom features to implement the site.
    'bulk_export',
    'hyperlocalnews_vocabularies',
    'hyperlocalnews_settings', 
    'hyperlocalnews_variables',
    'hyperlocalnews_front_page',
  );
}

/**
 * Implementation of hook_profile_tasks().
 *
 * (State machine modelled after Atrium installer.)
 */
function hyperlocalnews_profile_tasks(&$task, $url) {
  global $profile, $install_locale;

  $output = "";
  
  require_once('profiles/default/default.profile');

  watchdog('hyperlocal', $task);
  
  if ($task == 'profile') {

/*
 * These are good intentions, but generally broken and probably superceded by the features.
 *
    // Clean up node type settings not handled by features.
    // Default page to be NOT promoted.
    variable_set('node_options_page', array('status')); 
    variable_set('comment_page', COMMENT_NODE_READ_WRITE);

    // Default location to be NOT promoted.
    variable_set('node_options_location', array('status')); 
    variable_set('comment_page', COMMENT_NODE_READ_WRITE);
*/

    // Install the base module set.
    install_include(hyperlocalnews_profile_modules());

    //*** review permissions, reroll feature?
    // other variables?
    
    // Do the following before enabling the features because one feature adds Business to the menu.
    
    // Add Markdown filter to the top of the default input format.
    install_set_filter(1, "markdown", 0, -10);
    
    // Set zen as default theme.
    system_theme_data();
    install_enable_theme("zen");
    install_default_theme("zen");  

    // Enable much of the architecture via features.
    watchdog('hyperlocal', 'enabling feature modules');
    $modules =  _hyperlocalnews_additional_modules();
    foreach ($modules as $module) {
      _drupal_install_module($module);
      module_enable(array($module));  
    }

    watchdog('hyperlocal', 'adding About and help page, setting up front page');
    
    // TODO: A frontpage panel/view that is more awesome.
    //    variable_set (site_frontpage, "hln_panel");
    
    // This node must be in place before putting it into the menu.
    // Add a template About page. It will be nid 1 (for menu purposes).
    $node = array(
      'nid' => NULL,
      'title' =>  "About this site",
      'body' => "This is your site's About page, linked from the About menu.<br />
        Please edit the title and the body.",
      'type' => 'page',
      'teaser' => NULL,
      'log' => '',
      'created' => '',
      'format' => FILTER_FORMAT_DEFAULT,
      'uid' => 1,
      'status' => 1, // Published.
      'promote' => 0, // Not on front page.
    );
    $node = (object) $node;
    node_save($node);

    // Place instructions in an unpublished node that admins will see on the initial home page.
    $setup_body = 
'### _Hyperlocal News, a Drupal installation profile (preconfigured site)_

### Site setup notes

This node contains instructions for initial configuration of your Hyperlocal News site. Since it\'s unpublished, only administrators can see it. As you add content to the site this page will be pushed to the bottom.

The things you\'ll need to configure are listed below with links to pages for doing the configuration:

### Settings:

#### Host-specific:

* On some shared hosts, you\'ll see a message on the initial configuration page that "your server does not support clean URLs" 
  - don\'t be discouraged ... 
  - a subsequent visit to [the Clean URLs admin page](admin/settings/clean-urls) may display a message saying that your server *does* support clean URLs -- if so, select it and click Save
* [The directory where Drupal stores temporary files](/admin/settings/file-system) defaults to /tmp -- adjust if you are on a host without access to /tmp


#### General:

* Review the site info settings and upload images for [a site logo and a favicon](/admin/build/themes/settings/bakernow)
* Mollom (spam protection)
  - check [Mollom\'s settings](/admin/settings/mollom), and follow the directions to get and enter Mollom keys
* [Gmap](/admin/settings/gmap)
  - get and enter a Google Maps api key
  - use the interactive map to choose a center & default zoom (to match area in question), or enter latitude/longitude directly
* Google Analytics
  - enter [account number](/admin/settings/googleanalytics)
* [SMTP setup](/admin/settings/smtp)
  - enter SMTP server, authentication user/password, email from address/name
* Set [Newsletter options](/admin/content/simplenews/types)
* Review [everything under site information](/admin/settings/site-information)
* Edit the [About page](/node/1)
* [Set up contact form](/admin/build/contact/edit/1)
  - this is what provides the [Contact Us](/contact) link in the primary menu
  - it needs an email address and anything site-specific you want to say
* **Twitter module is broken, skip this step for now :(** Set up Twitter posting.
  - first decide [what type of posts the site should tweet](admin/settings/twitter/post)
  - next decide whether individual users should tweet with their own accounts, or whether the site should tweet everything with a master account
  - for per-user tweets:
    - each user should add Twitter account information to their [user profile](user) -- click Edit, and then visit the Twitter tab
    - users who have entered their Twitter account information into their user profile will post tweets when they enter content
  - for all site tweets:
    - configure a Twitter action starting on [the Actions administration page](admin/settings/actions)
    - select "Post a message to Twitter..." from the Advanced Action menu, click Create
    - enter Twitter credentials for site-wide posts and save
    - now set up how that action will be triggered on the [Triggers page](admin/build/trigger/node)
    - under "Trigger: After saving a new post" choose Post a message to Twitter and click the Assign button
* [Configure cron](http://drupal.org/getting-started/6/install/cron) -- necessary so your site looks after itself with periodic tasks.

### Personalize blocks:

* The [Blocks administration page](/admin/build/block) lists blocks of information on your site and where they will appear; some of these blocks are set to only appear on certain pages
* There are two custom blocks for which you can customize the content by clicking the Configure link on the respective entry; these are:
* Resources 
  - we had a link in here for the site\'s Twitter account 
  - site\'s RSS feed preconfigured
  - whatever else you like
  - since this is a custom block, the text & links are completely configurable, or you can turn the block off by moving it out of the "left sidebar" region

* Tip-join
  - this is the teaser below the header inviting visitors to send a news tip or join
  - like Resources this is a configurable custom block
  
### Taxonomy

* The Section vocabulary may be used to assign articles to sections on the site
  - you can add and delete Section terms on the [Section vocabulary page](/admin/content/taxonomy/2)
  - you can add a section to the top menu by adding an item on the [Primary Links administration page](/admin/build/menu-customize/primary-links); for example if you add a new item with the path set to "category/section/business", this will create a menu item showing all articles in the Business section of the site
* The Tags vocabulary will be populated as tags are added to content on the site

### Users & roles

* You can add more users and give registered users additional roles by visiting the [Users administration page](/admin/user/user)
* You can review the permissions granted to each role on the [Roles administration page](/admin/user/permissions) 
* The account you created during installation is a "super-user" and has permission to do anything on the site, including perform site updates
* It\'s a good idea to use the initial account only when needed, and to create another account with the "site admins" role for everyday use, including creating content

### Content

* Use the _Create content_ link in the left sidebar to add pages, locations, photos, etc.
* The Photos, Location Map and Recent Comments blocks will appear in the left sidebar as soon as some relevant content has been entered
* Add Locations to describe places in your area
  - these locations can then be referenced by typing their titles into the Location field of any other node
  - example: if you have already entered a location titled "Central Park" and you later add a Photo node, you can type "Central Park" into the location field as you add your photo
* Content types Page and Location are not promoted to the front page by default. Page is for things like the About page which get to through a menu. Locations are intended to be created as described above, so they dont need to appear on the front page.
';
    $node = array(
      'nid' => NULL,
      'title' =>  "Welcome to your Hyperlocal News site.",
      'body' => $setup_body,
      'type' => 'page',
      'teaser' => $setup_body,
      'log' => '',
      'created' => '',
      'format' => FILTER_FORMAT_DEFAULT,
      'uid' => 1,
      'status' => 0, // unpublished
      'promote' => 1,
    );
    $node = (object) $node;
    node_save($node);


    watchdog('hyperlocal', 'setting up terms & menu');
    // Add some categories to the Sections vocabulary. (Once the Sections vocabulary has been created by a feature.)
    $vid = install_taxonomy_get_vid('Section');
    install_taxonomy_add_term($vid, 'Business');
    install_taxonomy_add_term($vid, 'Arts');
    install_taxonomy_add_term($vid, 'Non-profit');
    install_taxonomy_add_term($vid, 'Gardening');
    install_taxonomy_add_term($vid, 'General site info');
    install_taxonomy_add_term($vid, 'News');
    install_taxonomy_add_term($vid, 'Public services');
    install_taxonomy_add_term($vid, 'Recreation');
    install_taxonomy_add_term($vid, 'Transportation');
    
    // Add About, Business, Locations, Photos, Contact Us to the primary links.
    install_contact_add_category("General Contacts or News Tips", "webmaster@example.com", '', 0, 1);
    variable_set('contact_form_information', 'You can leave a message or a news tip using the contact form below. Or you may <a href="/user/register">register</a> and post it yourself.');

    // Rebuild the menu cache so we can create the primary links.
    menu_rebuild();
 
    $items = array();
    $items['about'] = array(
      'link_path' => 'node/1',
      'link_title' => t('About'),
      'weight' => -10,
    ); 
    
    // TODO: this one isn't falling into place, despite the preceding menu_rebuild()
    // For now we work around it by including instructions for adding section terms to menu.
    $items['business'] = array(
      'link_path' => 'category/section/business',
      'link_title' => t('Business'),
      'weight' => -5,
    ); 
    $items['locations'] = array(
      'link_path' => 'locations',
      'link_title' => t('Locations'),
      'weight' => 0,
    );  
    $items['photos'] = array(
      'link_path' => 'photos',
      'link_title' => t('Photos'),
      'weight' => 5,
    );  
    $items['contact'] = array(
      'link_path' => 'contact',
      'link_title' => t('Contact Us'),
      'weight' => 10,
    );  
    // Create menu items
    foreach ($items as $item) {
      $item['menu_name'] = 'primary-links';
      $item['mlid'] = 0;
      $item['plid'] = 0;
      menu_link_save($item);
    }

    watchdog('hyperlocal', 'additional settings');
    // Make user 1 an administrator.
    $admin_rid = db_query("SELECT rid FROM {role} WHERE name ='administrator'");
    db_query("INSERT INTO {users_roles} VALUES (1, %d)", $admin_rid);
    
    // TODO: advanced help permissions for admin??
    
    // This is captured by strongarm, but then something else overrides it.
    variable_set("pathauto_node_image_pattern", "photos/[title-raw]");
    
    // TODO /admin/settings/location/geocoding, under USA choose Google Maps.
  
    // Set up blocks.
    system_initialize_theme_blocks('zen');
    $items = array();
    $theme = 'zen';
    
    // Custom block to invite anonymous users to send tips or join.
    $content = 'Got a news tip? You can <a href="/user/register">register</a> and post it yourself or <a href="/contact">contact us</a> to share the details and we\'ll post it for you.';
    $bid = install_create_custom_block($content, "tip-join", FILTER_FORMAT_DEFAULT);
    $delta = sprintf('%d', $bid);
    // Display for anonymous only.
    install_add_block_role('block', $delta, 1);
    
    $items['tip-join'] = new stdClass;
    $items['tip-join']->module = 'block';
    $items['tip-join']->delta = $delta;
    $items['tip-join']->weight = -5;
    $items['tip-join']->region = 'header';

    // All the following blocks are in left sidebar.
    $region = 'sidebar_first';
    
    // Custom resources block.
    // Use Full HTML to allow <img>.
    $format = 2;
    $content = '<ul>
<li><a href="http://twitter.com/replace_with_twitter_name">On twitter</a></li>
<li><a href="/rss.xml">This site\'s feed</a> <a href="/rss.xml" class="feed-icon"><img src="/misc/feed.png" alt="RSS Feed" width="16" height="16"></a></li>
</ul>';
    $bid = install_create_custom_block($content, "Resources", $format);
    $delta = sprintf('%d', $bid);
    
    $items['resources'] = new stdClass;
    $items['resources']->module = 'block';
    $items['resources']->delta = $delta;
    $items['resources']->weight = 15;
    $items['resources']->region = $region;
    $items['resources']->title = 'Local resources';
    
    // Position existing blocks and custom rules.
    
    // Recent photos.
    $items['photo-stream'] = new stdClass;
    $items['photo-stream']->module = 'views';
    $items['photo-stream']->delta = 'photo_stream-block_1';
    $items['photo-stream']->weight = -5;
    $items['photo-stream']->region = $region;
    // Don't display on photos page
     $items['photo-stream']->pages = 'photos';
    
    // Gmap locations.
    $items['locations'] = new stdClass;
    $items['locations']->module = 'views';
    $items['locations']->delta = 'locations-block_1';
    $items['locations']->weight = -10;
    $items['locations']->region = $region;
    // Don't display on locations or individual location pages
     $items['locations']->pages = "locations\nlocations/*";

    // Simplenews subscribe and recent issues.
    $items['simplenews'] = new stdClass;
    $items['simplenews']->module = 'simplenews';
    $items['simplenews']->delta = '1';
    $items['simplenews']->weight = 1;
    $items['simplenews']->region = $region;

    // Recent comments.
    $items['comment'] = new stdClass;
    $items['comment']->module = 'comment';
    $items['comment']->delta = '0';
    $items['comment']->weight = 5;
    $items['comment']->region = $region;
    
    // Content for this location.
    // This view's block has a very strange delta. But it works!
    $items['location-content'] = new stdClass;
    $items['location-content']->module = 'views';
    $items['location-content']->delta = '265d57928b343a5ac6dc3bdf8fa7f4d1';
    $items['location-content']->weight = -15;
    $items['location-content']->region = $region;
    // Show only on individual location pages
    $items['location-content']->visibility = 1;
    $items['location-content']->pages = 'locations/*';
    
    // Configure all the blocks.
    install_init_blocks();
    foreach ($items as $item) {
      $block = install_set_block($item->module, $item->delta, $theme, $item->region, $item->weight, $item->visibility, $item->pages, $item->custom, $item->throttle, $item->title);
    }

    // TODO: install phpmailer within sites/all/modules/smtp
    // Credit Hyperlocal News profile and GVS.
    variable_set('site_footer', 'Based on the Open Source <a href="http://drupal.org/project/hyperlocalnews">Hyperlocal News Drupal Profile</a> created by <a href="http://growingventuresolutions.com/node/1107">Growing Venture Solutions, LLC</a>.');

    // lowest priority:
    // TODO: make the taxonomy warnings at end of install go away
    
    watchdog('hyperlocal', 'finished custom tasks, flushing & rebuilding now');
    menu_rebuild();
    
    
    // Hand control back to installer.
    module_rebuild_cache(); 
    drupal_flush_all_caches();
    features_rebuild();
    $task = 'profile-finished';
    $output = "Site structure built.";
  }
  return $output;
}

// Set Hyperlocal News as default profile.
// (from Atrium installer: This is a trick for hooks to get called, otherwise we cannot alter forms.)
function system_form_install_select_profile_form_alter(&$form, $form_state) {
  foreach ($form['profile'] as $key => $element) {
    $form['profile'][$key]['#value'] = 'hyperlocalnews';
  }
}

