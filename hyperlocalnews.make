core = 6.19

; Following line comes out for d.o hosted version
projects[drupal] = 6.19


; Drupal contrib (specific version must be specified for d.o to package).
projects[install_profile_api] = 2.1
projects[advanced_help] = 1.2
projects[calendar] = 2.2 
projects[cck] = 2.8
projects[comment_notify] = 1.5
projects[date] = 2.6
projects[devel] = 1.22
projects[diff] = 2.1
projects[fasttoggle] = 1.3
projects[filefield] = 3.7
projects[globalredirect] = 1.2
projects[gmap] = 1.1
projects[google_analytics] = 2.3
projects[imageapi] = 1.9
projects[imagecache] = 2.0-beta10
projects[imagefield] = 3.7
projects[location] = 3.1
projects[markdown] = 1.2
projects[mollom] = 1.15
; dependency of two features, probably could be removed
projects[nodequeue] = 2.9

projects[path_redirect] = 1.0-rc1
projects[pathauto] = 1.5
projects[popups_reference] = 1.0
projects[popups] = 1.3
projects[porterstemmer] = 2.6
projects[simplenews] = 1.3
projects[token] = 1.15
; Twitter basic auth is no longer supported and oauth doens't work for actions. Sad panda.
; projects[twitter] = 3.0
; We can add back when http://drupal.org/node/404470 is fixed
projects[views] = 2.11
projects[jquery_ui] = 1.4
projects[features] = 1.0
projects[strongarm] = 2.0
projects[ctools] = 1.8
; We don't use context as its logic doesn't allow block placement like we need it
; projects[context] = 2.0
projects[exportables] = 2.0-beta1
projects[zen] = 2.0

; Libraries
libraries[jquery_ui][download][type] = "get"
libraries[jquery_ui][destination] = "modules/jquery_ui"
libraries[jquery_ui][download][url] = "http://jquery-ui.googlecode.com/files/jquery.ui-1.6.zip"
libraries[jquery_ui][directory_name] = "jquery.ui"

; Custom items pre-installed for now, pull from drupal.org later
; The hln feature module(s) ...
