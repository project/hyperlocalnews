<?php

/**
 * Helper to implementation of hook_ctools_plugin_api().
 */
function _hyperlocalnews_vocabularies_ctools_plugin_api() {
  $args = func_get_args();
  $module = array_shift($args);
  $api = array_shift($args);
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Helper to implementation of hook_strongarm().
 */
function _hyperlocalnews_vocabularies_strongarm() {
  $export = array();
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_extra_weights_business';
  $strongarm->value = array(
    'title' => '-5',
    'body_field' => '-1',
    'menu' => '-3',
    'taxonomy' => '-4',
  );

  $export['content_extra_weights_business'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_extra_weights_event';
  $strongarm->value = array(
    'title' => '-5',
    'body_field' => '-1',
    'menu' => '-3',
    'taxonomy' => '-4',
  );

  $export['content_extra_weights_event'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_extra_weights_image';
  $strongarm->value = array(
    'title' => '-5',
    'body_field' => '-1',
    'menu' => '-3',
    'taxonomy' => '-4',
  );

  $export['content_extra_weights_image'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_extra_weights_page';
  $strongarm->value = array(
    'title' => '-5',
    'body_field' => '-1',
    'menu' => '-3',
    'taxonomy' => '-4',
  );

  $export['content_extra_weights_page'] = $strongarm;
  return $export;
}
