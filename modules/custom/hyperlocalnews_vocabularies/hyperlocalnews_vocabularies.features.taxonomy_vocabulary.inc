<?php

/**
 * Helper to implementation of hook_taxonomy_default_vocabularies().
 */
function _hyperlocalnews_vocabularies_taxonomy_default_vocabularies() {
  $items = array(
  array(
      'name' => 'Section',
      'description' => '',
      'help' => '',
      'relations' => '1',
      'hierarchy' => '1',
      'multiple' => '0',
      'required' => '0',
      'tags' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
      'nodes' => array(
        'business' => 'business',
        'event' => 'event',
        'image' => 'image',
        'location' => 'location',
        'page' => 'page',
      ),
      'machine' => 'section',
    ),
  array(
      'name' => 'Tags',
      'description' => '',
      'help' => '',
      'relations' => '1',
      'hierarchy' => '0',
      'multiple' => '0',
      'required' => '0',
      'tags' => '1',
      'module' => 'taxonomy',
      'weight' => '0',
      'nodes' => array(
        'business' => 'business',
        'event' => 'event',
        'image' => 'image',
        'location' => 'location',
        'page' => 'page',
      ),
      'machine' => 'tags',
    ),
  );
  return $items;
}

/**
 * Helper to implementation of hook_enable().
 */
function _hyperlocalnews_vocabularies_enable() {
  exportables_sync('taxonomy_vocabulary', 'hyperlocalnews_vocabularies');
}

/**
 * Helper to implementation of hook_disable().
 */
function _hyperlocalnews_vocabularies_disable() {
  exportables_unsync('taxonomy_vocabulary', 'hyperlocalnews_vocabularies');
}
