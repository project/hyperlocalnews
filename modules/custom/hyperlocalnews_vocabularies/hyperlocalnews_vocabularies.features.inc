<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function hyperlocalnews_vocabularies_ctools_plugin_api() {
  module_load_include('inc', 'hyperlocalnews_vocabularies', 'hyperlocalnews_vocabularies.defaults');
  $args = func_get_args();
  return call_user_func_array('_hyperlocalnews_vocabularies_ctools_plugin_api', $args);
}

/**
 * Implementation of hook_taxonomy_default_vocabularies().
 */
function hyperlocalnews_vocabularies_taxonomy_default_vocabularies() {
  module_load_include('inc', 'hyperlocalnews_vocabularies', 'hyperlocalnews_vocabularies.features.taxonomy_vocabulary');
  $args = func_get_args();
  return call_user_func_array('_hyperlocalnews_vocabularies_taxonomy_default_vocabularies', $args);
}

/**
 * Implementation of hook_enable().
 */
function hyperlocalnews_vocabularies_enable() {
  module_load_include('inc', 'hyperlocalnews_vocabularies', 'hyperlocalnews_vocabularies.features.taxonomy_vocabulary');
  $args = func_get_args();
  return call_user_func_array('_hyperlocalnews_vocabularies_enable', $args);
}

/**
 * Implementation of hook_disable().
 */
function hyperlocalnews_vocabularies_disable() {
  module_load_include('inc', 'hyperlocalnews_vocabularies', 'hyperlocalnews_vocabularies.features.taxonomy_vocabulary');
  $args = func_get_args();
  return call_user_func_array('_hyperlocalnews_vocabularies_disable', $args);
}

/**
 * Implementation of hook_strongarm().
 */
function hyperlocalnews_vocabularies_strongarm() {
  module_load_include('inc', 'hyperlocalnews_vocabularies', 'hyperlocalnews_vocabularies.defaults');
  $args = func_get_args();
  return call_user_func_array('_hyperlocalnews_vocabularies_strongarm', $args);
}
