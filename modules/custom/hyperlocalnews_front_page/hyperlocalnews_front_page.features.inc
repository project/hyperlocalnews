<?php

/**
 * Implementation of hook_views_default_views().
 */
function hyperlocalnews_front_page_views_default_views() {
  module_load_include('inc', 'hyperlocalnews_front_page', 'hyperlocalnews_front_page.features.views');
  $args = func_get_args();
  return call_user_func_array('_hyperlocalnews_front_page_views_default_views', $args);
}
