<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function hyperlocalnews_variables_ctools_plugin_api() {
  module_load_include('inc', 'hyperlocalnews_variables', 'hyperlocalnews_variables.defaults');
  $args = func_get_args();
  return call_user_func_array('_hyperlocalnews_variables_ctools_plugin_api', $args);
}

/**
 * Implementation of hook_strongarm().
 */
function hyperlocalnews_variables_strongarm() {
  module_load_include('inc', 'hyperlocalnews_variables', 'hyperlocalnews_variables.defaults');
  $args = func_get_args();
  return call_user_func_array('_hyperlocalnews_variables_strongarm', $args);
}
