<?php

/**
 * Implementation of hook_content_default_fields().
 */
function hyperlocalnews_settings_content_default_fields() {
  module_load_include('inc', 'hyperlocalnews_settings', 'hyperlocalnews_settings.defaults');
  $args = func_get_args();
  return call_user_func_array('_hyperlocalnews_settings_content_default_fields', $args);
}

/**
 * Implementation of hook_ctools_plugin_api().
 */
function hyperlocalnews_settings_ctools_plugin_api() {
  module_load_include('inc', 'hyperlocalnews_settings', 'hyperlocalnews_settings.defaults');
  $args = func_get_args();
  return call_user_func_array('_hyperlocalnews_settings_ctools_plugin_api', $args);
}

/**
 * Implementation of hook_imagecache_default_presets().
 */
function hyperlocalnews_settings_imagecache_default_presets() {
  module_load_include('inc', 'hyperlocalnews_settings', 'hyperlocalnews_settings.defaults');
  $args = func_get_args();
  return call_user_func_array('_hyperlocalnews_settings_imagecache_default_presets', $args);
}

/**
 * Implementation of hook_node_info().
 */
function hyperlocalnews_settings_node_info() {
  module_load_include('inc', 'hyperlocalnews_settings', 'hyperlocalnews_settings.features.node');
  $args = func_get_args();
  return call_user_func_array('_hyperlocalnews_settings_node_info', $args);
}

/**
 * Implementation of hook_user_default_permissions().
 */
function hyperlocalnews_settings_user_default_permissions() {
  module_load_include('inc', 'hyperlocalnews_settings', 'hyperlocalnews_settings.defaults');
  $args = func_get_args();
  return call_user_func_array('_hyperlocalnews_settings_user_default_permissions', $args);
}

/**
 * Implementation of hook_strongarm().
 */
function hyperlocalnews_settings_strongarm() {
  module_load_include('inc', 'hyperlocalnews_settings', 'hyperlocalnews_settings.defaults');
  $args = func_get_args();
  return call_user_func_array('_hyperlocalnews_settings_strongarm', $args);
}

/**
 * Implementation of hook_views_default_views().
 */
function hyperlocalnews_settings_views_default_views() {
  module_load_include('inc', 'hyperlocalnews_settings', 'hyperlocalnews_settings.features.views');
  $args = func_get_args();
  return call_user_func_array('_hyperlocalnews_settings_views_default_views', $args);
}
